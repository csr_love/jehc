#  **QQ群：102642604** 


**本工程为Maven多工程依赖版本** 
统一开发 统一开放 统一规范 统一开源
http://git.oschina.net/jehc/jehc


 **开发工具：** 
 **eclipse-jee-mars-1** 
 **或eclipse-jee-mars-2** 
 **或eclipse-juno** 
 **或STS** 


 **非maven版本地址：** 

 **https://gitee.com/jehc/jehc-none-maven** 

单工程版本如下：
 **https://gitee.com/jehc/jehc-admin** 


采用的技术如下：

框架基础后端技术：Spring+SpringMVC+Mybatis

框架基础前端技术：Jquery+Bootstrap+Extjs6.2.1+Mxgraph(流程设计器)等

框架其它技术：接口采用Swagger2，全文检索：solr4.10，工作流引擎Activiti5.22，缓存框架：【Redis，Ehcache】，Logback，FTP，hessian，FastJSON,GZIP(TK技术)，quartz，消息中间件RabbitMq+Kafka，及时通讯NETTY,分页插件PageHelper,SpringPool

框架数据库支持：Mysql5.6，Oracle11g

框架基础功能：

1. 用户中心
2. 公司信息
3. 部门
4. 岗位
5. 角色权限
6. 组织机构（对部门岗位合并操作）
7. 数据权限、
8. 全文检索
9. 流程关联
10. 流程在线设计
11. 我的任务
12. 平台公告
13. 平台通知
14. 代码生成器
15. 业务日志记录
16. 页面性能日志监控
17. 操作日志
18. 平台日志记录
19. 菜单配置、
20. 功能配置
21. 公告功能配置
22. JSRUN
23. 数据库备份
24. 数据库维护
25. FlexSeach
26. 平台统一附件
27. 调度器维护
28. 调度器执行操作


框架其它功能：
    电商后端
    
效果图片如下：

jEhc能做什么
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210849_d16c8e6d_1341290.png "首页.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/205805_a19f4919_1341290.png "设计器.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210706_c3cf0a3e_1341290.png "流程中心.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210039_38616b78_1341290.png "角色权限.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210315_2b221424_1341290.png "分配用户.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/205936_60584a04_1341290.png "用户体系.png")

只列举小部分功能如下：

![输入图片说明](https://gitee.com/uploads/images/2018/0410/210159_7c3098ea_1341290.png "各种功能.png")

电商后端及前端
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210458_d5472108_1341290.png "购物车.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0410/210418_1846ff18_1341290.png "订单.png")


统一开发 统一开放 统一规范 统一开源
 **http://git.oschina.net/jehc/jehc** 


 **开发工具：** 
 **eclipse-jee-mars-1
或eclipse-jee-mars-2
或eclipse-juno
或STS** 

 **非maven版本地址：** 

 **https://gitee.com/jehc/jehc-none-maven** 

 
单工程版本如下：
 **https://gitee.com/jehc/jehc-admin** 